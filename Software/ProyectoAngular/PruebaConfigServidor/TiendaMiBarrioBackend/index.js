//Forma convencional de ES6 => estándar convencional de JS
//import express from express
//Forma sistema nativo de NodeJS
const express = require('express');
//Importar m[odulo de MongoDB => mongoose
const mongoose = require('mongoose');

const router = express.Router();

let appExpress = express();

// appExpress.use('/', function(request, response){
//     response.send("Hola mundo");
// });

appExpress.use(router);
appExpress.listen(4000);

//get
router.get('/buscarget', function(req, res){
    res.send("Prueba metodo get ");
});
//post get
router.post('/buscarget', function(req, res){
    res.send("Prueba metodo post con get");
});

//post
router.post('/crearpost', function(req, res){
    res.send("Prueba metodo post");
});

//put
router.put('/actualizarput', function(req, res){
    res.send("Prueba metodo put");
});

//delete
router.delete('/eliminardelete', function(req, res){
    res.send("Prueba metodo delete");
});

//Conexion con la base de datos
const user = 'B30G3TiendaVirtualUser';
const password = 'admin';
const db = 'tiendaVirtualRemote';
const url = `mongodb+srv://${user}:${password}@cluster-misiontic-tiend.9px6z.mongodb.net/${db}?retryWrites=true&w=majority`;

mongoose.connect(url)
    .then(function(){
        console.log("Conectado a MongoDB")
    }).catch(function(e)
        {console.log(e)
    });

console.log("La aplicacion se ejecuta en http://localhost:4000 ");